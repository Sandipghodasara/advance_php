<?php

$unsorted_numbers = array(3,7,1,26,43,12,6,21,23,73);

function quicksort($array)
{
    if (count($array) == 0)
        return array();

    $pivot_element = $array[0]; //select a PIVOT value (its used to compare other element with ref to this value and dived array into 2 parts )
    $left_element = $right_element = array(); // creat a empty array left side and right side

    //this loop checked all element with reference to PIVOT value and and dived into left and right array
    for ($i = 1; $i < count($array); $i++) {
        if ($array[$i] <$pivot_element)
            $left_element[] = $array[$i];
        else
            $right_element[] = $array[$i];
    }

    //recursively passed both array and return it;
    return array_merge(quicksort($left_element), array($pivot_element), quicksort($right_element));
}

$sorted_numbers = quicksort($unsorted_numbers);

print_r($sorted_numbers);

?>