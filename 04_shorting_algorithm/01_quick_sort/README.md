> QUICK SORT




Quicksort Algorithm is based on partitioning of array of data into smaller arrays. And select the Highest index value as a Pivot element. Take two variables to point left and right of the list excluding pivot.Left points to the lower index than pivot element.Right points are higher than Pivot element.while value at left is less than pivot element move right else move left.





```
user_array = array(3,7,1,26,43,12,6,21,23,73);

    make a function quicksort($e){
        if array length is 0 return array
        if array length is > 0

        saved first element as a pivot or starting value like => {{ start = e[0] }}
        now make a two empty array like => {{ left_side = right_side = array() }}

        --for loop to compare values--
        for (index = 0; index < count(e); index++ ){
            now compare array[index] with pivot value if value is less than pivot value{
                 push array[index] to left_side array
            }if value is greater than pivot value{
                 push array[index] to right_side array
            }
        }

        now using function as a recursion passed left_side array and right_side array than and than so on.....
        and finally return final array   
        {{ return array_merge(quicksort(left_element), array(pivot value), quicksort(right_element))
    }
```