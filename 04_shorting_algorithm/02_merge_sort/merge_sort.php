<?php

function mergeSort($array)
{

    // first dived array into 2 parts and recursively until array hase one element;
    if(count($array) == 1 )
    {
        return $array;
    }

    $mid = count($array) / 2; // find mid element
    $left = array_slice($array, 0, $mid); // with ref of mid, store remaining left side array into left array
    $right = array_slice($array, $mid); // with ref of mid, store remaining right side array into right array
    $left = mergeSort($left); // recursively for left array until array has one element
    $right = mergeSort($right); // recursively for right array until array has one element

    return merge($left, $right);
}


function merge($left, $right)
{
    // creating a empty array for stored a value
    $res = array();

    //this loop compare left size array and right side array, if one array has more element compare to 2nd one its compare only same index
    while (count($left) > 0 && count($right) > 0)
    {
        if($left[0] > $right[0])  // left index is greater than right index than store right index value into new array
        {
            $res[] = $right[0];
            $right = array_slice($right , 1);
        }
        else
        {
            $res[] = $left[0];
            $left = array_slice($left, 1);
        }
    }

    //this loop checked remaining element which is not compere in upper condition for left side and right side
    while (count($left) > 0) // this is for left side
    {
        $res[] = $left[0];
        $left = array_slice($left, 1);
    }

    while (count($right) > 0) // this is for right side
    {
        $res[] = $right[0];
        $right = array_slice($right, 1);
    }

    return $res;
}

print_r( mergeSort(array(3,7,1,26,43,12,6,21,23,73)))

?>
