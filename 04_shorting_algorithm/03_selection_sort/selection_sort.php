<?php

$unsorted_numbers = array(3,7,1,26,43,12,6,21,23,73);

function selectionsort($array){

    //loop which will start from index 0
    for($i = 0; $i < count($array) ; $i++) {
        $min_key = $i; // store 1st element as a min_key

        //compare array[j] with array[min_key] and update min_key
        for($j = $i + 1; $j < count($array) ; $j++) {
            if ($array[$j] < $array[$min_key]) {
                $min_key = $j;
            }
        }

        //this condition is for swapping
        if ($array[$i] > $array[$min_key]) {
            $tmp = $array[$i];
            $array[$i] = $array[$min_key];
            $array[$min_key] = $tmp;
        }
    }
    return $array;
}

$sorted_numbers = selectionsort($unsorted_numbers);

print_r($sorted_numbers);

?>