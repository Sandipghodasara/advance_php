> INSERTION SORT

The selection sort algorithm sorts an array by repeatedly finding the minimum element (considering ascending order) from unsorted part and putting it at the beginning. The algorithm maintains two subarrays in a given array.

1) The subarray which is already sorted.
2) Remaining subarray which is unsorted.

In every iteration of selection sort, the minimum element (considering ascending order) from the unsorted subarray is picked and moved to the sorted subarray.


```
user_array = array(3,7,1,26,43,12,6,21,23,73);

    function selectionsort(array){
    
    //loop which will start from index 0 
    for(i = 0; i < count(array) ; i++) {
        minkey = i; // select i as a minkey
        
        //compare array[j] with array[minkey] and update minkey
        for(j = i + 1; j < count(array) ; j++) { 
            if (array[j] < array[minkey]) {
                minkey = j;  
            }
        }

        //swap array element
        if (array[i] > array[minkey]) {
            tmp = array[$i];
            array[i] = array[minkey];
            array[minkey] = tmp;
        }
    }
    return $array;
}
```