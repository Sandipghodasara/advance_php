<?php
$unsorted_numbers = array(3,7,1,26,43,12,6,21,23,73);

function bubblesort($array)
{
    //loop through all array element
    for ($i = 0; $i < count($array); $i++) {
        // loop that go last to prev value because Last i elements are already in place
        for ($j = 0; $j < (count($array)-$i)-1; $j++){
            //this loop is for swapping
            if($array[$j] > $array[$j+1]){
                $min = $array[$j];
                $array[$j] = $array[$j+1];
                $array[$j+1] = $min;
            }
        }
     }

    return $array;
}

$sorted_numbers = bubblesort($unsorted_numbers);

print_r($sorted_numbers);

?>