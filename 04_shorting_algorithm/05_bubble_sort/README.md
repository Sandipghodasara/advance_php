> BUBBLE SORT

Bubble Sort is the simplest sorting algorithm that works by repeatedly swapping the adjacent elements if they are in wrong order.



```
user_array = array(3,7,1,26,43,12,6,21,23,73);

    make a function bubblesort(arr){
        
    // Traverse through all array elements 
    for(i = 0; i < count(arr); i++){ 
    
        // Last i elements are already in place 
        for (j = 0; j < (count(arr) - i)-1; j++){ 
        
            // traverse the array from 0 to (count(arr)-i)-1 
            // Swap if the element found is greater 
            // than the next element 
            if (arr[$j] > arr[$j+1]){ 
                temp = arr[j]; 
                arr[j] = arr[$+1]; 
                arr[j+1] = temp; 
            } 
        } 
    } 
} 
```