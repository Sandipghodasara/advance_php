> INSERTION SORT

Insertion sort is a simple sorting algorithm that works similar to the way you sort playing cards in your hands. The array is virtually split into a sorted and an unsorted part. Values from the unsorted part are picked and placed at the correct position in the sorted part.

>*Algorithm*

To sort an array of size n in ascending order: \
1: Iterate from arr[1] to arr[n] over the array. \ 
2: Compare the current element (key) to its predecessor. \
3: If the key element is smaller than its predecessor, compare it to the elements before. Move the greater elements one position up to make space for the swapped element.


```
user_array = array(3,7,1,26,43,12,6,21,23,73);

    function insertionsort(array){

    loop which will start from index 1 
    for(i=1; i < count(array); i++){
        key = array[i] // value of current element
        j = i-1  // store one element before the current element

        checking the all other element with ref to key and j
        while( j >=0 && array[j] > key){
            if j is greater than 0 and array[j] is greater than key than value array[j+1] = array[j] 
            j = j-1;
        }
        
        store the value of key in the last index that is j
        array[j+1] = key;
    }
    
    return array;
}
```