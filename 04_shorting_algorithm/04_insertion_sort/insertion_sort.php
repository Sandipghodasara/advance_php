<?php
$unsorted_numbers = array(3,7,1,26,43,12,6,21,23,73);

function insertionsort($array)
{
    //loop which will start from index 1 for storing key
    for ($i = 1; $i < count($array); $i++) {
        $key = $array[$i];
        $j = $i - 1;

        //checking the all other element with ref to key and j
        while ($j>=0 && $array[$j]>$key){ // if j is greater than or equal 0 and array[j] is greater than key than value array[j+1] = array[j]
            $array[$j+1] = $array[$j];
            $j = $j-1;
        }

        //to swapping key to final position
        $array[$j+1] = $key;

     }

    return $array;
}

$sorted_numbers = insertionsort($unsorted_numbers);

print_r($sorted_numbers);

?>