<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Final</title>
</head>
<body>
    <?php
        session_start();
        if(isset($_POST["submit"])){
            header("Location: 02_sign_in.php");
            session_destroy();
        }
    ?>

    <form action="#" method="post">
        <h1 class="user">
            <?php
            if(!isset($_COOKIE["username"])){
                header("Location: 02_sign_in.php");
                session_destroy();
            }
            echo $_COOKIE["username"];
            ?>
        </h1>
        <button type="submit" name="submit" class="btn">LOGOUT</button>
    </form>
</body>
</html>
