<?php

$f_nameErr=$l_nameErr=$contactErr=$emailErr=$passwordErr=$uploadErr= "";
$flag = 0;

if ($_POST){
    if (empty($_POST["f_name"])){
      $f_nameErr = "Enter First Name";
        $flag = 1;
    }else{
      $f_name = $_POST["f_name"];
      $rexName = "/^[a-zA-Z]*$/";
      if(!preg_match($rexName, $f_name)){
          $f_nameErr = "Enter Valid First Name";
          $flag = 1;
      }
    }

    if (empty($_POST["l_name"])){
        $l_nameErr = "Enter Last Name";
        $flag = 1;
    }else{
        $l_name = $_POST["l_name"];
        $rexName = "/^[a-zA-Z]*$/";
        if(!preg_match($rexName, $l_name)){
            $l_nameErr = "Enter Valid Last Name";
            $flag = 1;
        }
    }

    if (empty($_POST["contact"])){
        $contactErr = "Enter contact No.";
        $flag = 1;
    }else{
        $contact = $_POST["contact"];
        $rexName = "/^[0-9]{10}$/";
        if(!preg_match($rexName, $contact)){
            $contactErr = "Enter Valid contact No.";
            $flag = 1;
        }
    }

    if(empty($_POST["email"])){
        $emailErr = "Enter email";
        $flag = 1;
    }else{
        $email = $_POST["email"];
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format";
            $flag = 1;
        }
    }

    if (empty($_POST["password"])){
        $passwordErr = "Enter Password";
        $flag = 1;
    }else{
        $password = $_POST["password"];
        $rexName = "/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/"; //Minimum eight characters, at least one letter and one number
        if(!preg_match($rexName, $password)){
            $passwordErr = "Enter Valid Password";
            $flag = 1;
        }
    }

    if($flag !== 1){
        $target_dir = "./image_store/";
        $target_file = $target_dir . basename($_FILES["file"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $check = getimagesize($_FILES["file"]["tmp_name"]);

        if ($check !== false){
            if ($imageFileType != "jpg"){
                $uploadErr = "Upload file with valid Extension (JPEG/JPG)";
            }else{
                if ($_FILES["file"]["name"] > 500000){
                    $uploadErr = "File Size Should be Less Than 5M";
                }else{
                    if (file_exists($target_file)){
                        $uploadErr = "File Already Exists";
                    }else{
                        move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
                        $data = json_encode($_POST);
                        $fileOpen = fopen("data.txt", "a") or die ("Unable to open");
                        fwrite($fileOpen, PHP_EOL.$data);
                        fclose($fileOpen);
                        header("Location: 02_sign_in.php");
                    }
                }
            }
        }
    }
}

?>

