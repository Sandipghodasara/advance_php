
<?php

include "04_sign_in_val.php";
if(isset($_SESSION["email"])){
    header("Location: 05_success.php");
}
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./css/style.css">
    <title>Registration From</title>
</head>
<body>

<div class="wrapper">
    <h1 class="head__name text__center">Sign In</h1>
    <form action="#" method="post" enctype="multipart/form-data">

        <label for="email">Email</label>
        <input type="email" id="email" name="email" value="<?php echo isset($email) ? $email : "" ; ?>">
        <span> <?php echo $emailErr ?> </span>
        <pre></pre>

        <label for="password">Password</label>
        <input type="password" id="password" name="password" value="">
        <span> <?php echo $passwordErr ?> </span>
        <pre></pre>

        <button type="submit" class="btn btn__sign__up font__white">Sign In</button>
        <a href="01_sign_up.php" class="btn">Sign Up</a>

    </form>
</div>


</body>
</html>
