<?php
include "03_sign_up_val.php";
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./css/style.css">
    <title>Registration From</title>
</head>
<body>

    <div class="wrapper">
        <h1 class="head__name text__center">Sign Up</h1>
        <form action="#" method="post" enctype="multipart/form-data">
            <label for="f_name">First Name</label>
            <input type="text" id="f_name" name="f_name" value="<?php echo isset($f_name) ? $f_name : "" ; ?>">
            <span> <?php echo $f_nameErr ?> </span>
            <pre></pre>

            <label for="l_name">Last Name</label>
            <input type="text" id="l_name" name="l_name" value="<?php echo isset($l_name) ? $l_name : "" ; ?>">
            <span> <?php echo $l_nameErr ?> </span>
            <pre></pre>

            <label for="contact">Contact No.</label>
            <input type="number" id="contact" name="contact" value="<?php echo isset($contact) ? $contact : "" ; ?>">
            <span> <?php echo $contactErr ?> </span>
            <pre></pre>

            <label for="email">Email</label>
            <input type="email" id="email" name="email" value="<?php echo isset($email) ? $email : "" ; ?>">
            <span> <?php echo $emailErr ?> </span>
            <pre></pre>

            <label for="password">Password</label>
            <input type="password" id="password" name="password" value="">
            <span> <?php echo $passwordErr ?> </span>
            <pre></pre>

            <label for="file">Upload Image</label>
            <input type="file" id="file" name="file">
            <span> <?php echo $uploadErr ?> </span>
            <pre></pre>

            <button type="submit" class="btn btn__sign__up font__white">Sign Up</button>
            <a href="02_sign_in.php" class="btn">Sign In</a>

        </form>
    </div>


</body>
</html>
