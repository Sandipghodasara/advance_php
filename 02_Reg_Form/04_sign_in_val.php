<?php
session_start();
$emailErr=$passwordErr= "";
$flag = 0;

if ($_POST){
    if (empty($_POST["email"])){
        $emailErr = "Enter email";
        $flag = 1;
    }else{
        $email = $_POST["email"];
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format";
            $flag = 1;
        }
    }

    if (empty($_POST["password"])){
        $passwordErr = "Enter Password";
        $flag = 1;
    }else{
        $password = $_POST["password"];
        $rexName = "/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/"; //Minimum eight characters, at least one letter and one number
        if(!preg_match($rexName, $password)){
            $passwordErr = "Enter Valid Password";
            $flag = 1;
        }
    }

    if($flag == 0){
        $file = fopen("data.txt", "r") or die ("Unable to open");
        while (!feof($file)){
            $line = fgets($file);
            $user_data = json_decode($line, true);
            if($user_data["email"] == $email && $user_data["password"] == $password){
                $_SESSION["email"] = $email;
                setcookie("username", $user_data["f_name"], time()+100);
                header("Location: 05_success.php");
                echo "in";
            }
        }
        fclose($file);
    }



}

?>

