<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PT3</title>
</head>
<body>

<form action="#" method="post">
    <input type="number" name="number" >
    <button type="submit" name="submit"> Hit Enter</button>
</form>

<?php
if($_POST){
    $num = $_POST["number"];
    for ($i = (-$num); $i < $num; $i++) { // loop for row
        for ($j = (-$num); $j <= $num; $j++){ // loop for col
            if (abs($j) == 0) continue; //this is skip for 0th line
            if(abs($j)<=abs($i)){
                echo "&nbsp;&nbsp";  //this is for spacing
            }else{
                echo "*"; //its for printing *
            }
        }
        echo "<br>";
    }
}
?>

</body>
</html>
