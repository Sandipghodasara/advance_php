<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PT2</title>
</head>
<body>

<form action="#" method="post">
    <input type="number" name="number" >
    <button type="submit" name="submit"> Hit Enter</button>
</form>

<?php
if($_POST){
    $num = $_POST["number"];
    for ($i = 0; $i < $num; $i++){ // loop for row
        for ($j = (-$num*2); $j <= ($num*2); $j++){ // loop for col
            if(abs($j) <= (($num*2)-$i) && (abs($j) >= (($num*2)) - 2*$i) || $i >= (2*$num)/2){ //this condition for printing *
                echo "*";
            }elseif (abs($j)<$i+1){
                echo "*";
            }else{
                echo "&nbsp;&nbsp";  //this is for printing "" <- space
            }
        }
        echo "<br>";
    }
}


?>

</body>
</html>
