<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PT1</title>
</head>
<body>

    <form action="#" method="post">
        <input type="number" name="number" >
        <button type="submit" name="submit"> Hit Enter</button>
    </form>

    <?php
        if($_POST) {
            $num = $_POST["number"];
            for ($i = 0; $i < $num; $i++) { // loop for row
                if (($num - $i) >= 10) { //condition for spacing
                    for ($k = 0; $k < $i; $k++) {
                        echo "&nbsp;&nbsp;";
                    }
                } else {
                    echo str_repeat("&nbsp;&nbsp", $num - 9);
                }

                for ($j = -$num; $j <= $num; $j++) { // loop for col
                    if ($j == 0) {
                        continue;
                    } elseif (abs($j) <= $i) {
                        echo " * "; //for printing *
                    } else {
                        echo "  " . ($num - abs($j) + 1) . "  "; //for printing numbers
                    }
                }
                echo "<br>";
            }
        }
    ?>

</body>
</html>
