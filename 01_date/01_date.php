<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<?php
    //date and time
    echo "Date <br>";
    echo "date " . date("Y-m-d") . "<br>";
    echo "date " . date("Y-M-d") . "<br>";
    echo "date " . date("y-M-D") . "<br>";
    echo "date " . date("Y-M-D") . "<br>";
    echo "<br>";
    echo "<br>";
    echo "Time <br>";
    echo "time " . date("H:i:s") ." <= 24 hour" . "<br>";
    echo "time " . date("h:i:s") ." <= 12 hour" . "<br>";
    echo "time " . date("h:i:s A") ." <= Am/Pm small" . "<br>";
    echo "time " . date("h:i:s a") ." <= am/pm small" . "<br>";
    echo "<br>";
    echo "<br>";
    echo "Make Time <br>";
    $mk_date = mktime(10,20,45,1,4,2021);
    echo "mk_date " . date("Y-m-d", $mk_date) . "<br>";
    echo "<br>";
    echo "<br>";
    echo "string to time <br>";
    $mk_date = strtotime("10:20:45 Jan-05-2021");
    echo "mk_date " . date("Y-m-d", $mk_date) . "<br>";

    $mk_date = strtotime("yesterday");
    echo "yesterday_date " . date("Y-m-d", $mk_date) . "<br>";

    $mk_date = strtotime("now");
    echo "Now_or_Today_date " . date("Y-m-d", $mk_date) . "<br>";

    $mk_date = strtotime("tomorrow");
    echo "Tomorrow_date " . date("Y-m-d", $mk_date) . "<br>";
?>

    <h1>Anniversary Cal</h1>
    <form action="#" method="post">
        <input class="date1" type="date" name="date1"> Date-1
        <br>
        <input class="date2" type="date" name="date2"> Date-2
        <pre></pre>
        <button class="submit" type="submit" name="submit">Hit Enter..!</button>
    </form>
    <pre></pre>

<?php
    if($_POST){
        $date1 = strtotime($_POST["date1"]);
        $date2 = strtotime($_POST["date2"]);
        if(isset($_POST["submit"])){
            $remain = abs(ceil(($date2-$date1)/60/60/24));
            echo date("Y-m-d", $date1)." ". date("D", $date1) ."<br>";
            echo date("Y-m-d", $date2)." ". date("D", $date2) ."<br>";
            echo "There are " . $remain." days until my Anniversary". "<br>";
            echo "There are " . ceil( $remain*24). " Hours until my Anniversary". "<br>";
            echo "There are " . ceil( $remain*24*60). " Minutes until my Anniversary". "<br>";
        }
    }
?>

</body>
</html>